#!/usr/bin/env bash
rm -f demo/demo.db
rm -rf media/*
rm -rf static/*
source venv/bin/activate
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic
python manage.py createsuperuser
python manage.py migrate
python manage.py runserver