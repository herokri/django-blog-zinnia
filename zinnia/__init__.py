"""Zinnia"""
__version__ = '1.02'
__license__ = 'BSD License'

__author__ = 'Fantomas42'
__email__ = 'fantomas42@gmail.com'

__url__ = 'https://gitlab.com/herokri/django-blog-zinnia'

default_app_config = 'zinnia.apps.ZinniaConfig'
