# Generated by Django 2.1.5 on 2019-02-08 15:01

from django.db import migrations
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('zinnia', '0006_entry_ppoi'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='image',
            field=versatileimagefield.fields.VersatileImageField(blank=True, help_text='Used for illustration.', upload_to='blog', verbose_name='image'),
        ),
    ]
